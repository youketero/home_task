package epam.com.arrayDublicate;

import java.util.ArrayList;
import java.util.Collection;

public class Main {
    public static void main(String[] args) {
        Collection arr = new ArrayList<>();
        arr.add(1.123);
        arr.add(2.123);
        arr.add(2.123);
        arr.add(4);
        arr.add(2);
        arr.add(2.123);
        arr.add(13);
        arr.add("dog");
        arr.add("cat");
        arr.add("dog");
        System.out.println(duplicateArray((ArrayList) arr));
    }

    public static ArrayList duplicateArray(ArrayList arg) {
        int counter = 0;
        for (int i = 0; i < arg.size(); i++) {
            if (arg.indexOf(arg.get(i)) == arg.lastIndexOf(arg.get(i))) {
                Object arg1 = arg.get(counter);
                arg.set(counter++, arg.get(i));
                arg.set(i, arg1);
            }
        }
        return arg;
    }
}
