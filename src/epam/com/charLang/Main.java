package epam.com.charLang;

public class Main {
    public static void main(String[] args) {
        System.out.println(getChar(1));
        System.out.println(getChar(3));
        System.out.println(getChar(9));
        System.out.println(getChar(-5));
        System.out.println(getChar(0));
    }

    public static char getChar(int i) {
        if ((i > 0) && (i < 26)) {
            return (char) (i + 96);
        }
        return '0';
    }
}
