package epam.com.palindromSecondVersion;

public class Main {
    public static void main(String[] args) {
        System.out.println(checkPalindrom("Never odd or even"));
        System.out.println(checkPalindrom(""));
    }

    public static String checkPalindrom(String str) {
        if (str.length() == 0 || str.length() == 1) {
            return "String is empty or have 1 symbol";
        }
        String strToLowerCase = str.replaceAll("\\s+", "").toLowerCase();
        int lastIndex = strToLowerCase.length() - 1;
        return recursivePalindrom(strToLowerCase, 0, lastIndex, str);
    }

    public static String recursivePalindrom(String str, int firstIndex, int lastIndex, String str1) {
        if (firstIndex == lastIndex) {
            return "Not a palindrom";
        }
        if ((str.charAt(firstIndex)) != (str.charAt(lastIndex))) {
            return "Not a palindrom";
        }
        if (firstIndex < lastIndex + 1) {
            return recursivePalindrom(str, firstIndex + 1, lastIndex - 1, str1);
        }
        return str1.substring(0, str1.length() / 2);
    }

}
