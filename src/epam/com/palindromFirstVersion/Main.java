package epam.com.palindromFirstVersion;

public class Main {
    public static void main(String[] args) {
        System.out.println(isPalindrom("Never odd or even"));
        System.out.println(isPalindrom(""));
        System.out.println(isPalindrom("ab"));
    }

    public static String isPalindrom(String str) {
        if (str.length() == 0 || str.length() == 1) {
            return "String is empty or have 1 symbol";
        }
        StringBuilder newStr = new StringBuilder();
        String strToLowerCase = str.replaceAll("\\s+", "").toLowerCase();
        char[] strChar = strToLowerCase.toCharArray();
        for (int i = 0; i <= strChar.length - 1; i++) {
            newStr.append(strChar[strChar.length - 1 - i]);
        }
        if (newStr.toString().equals(strToLowerCase)) {
            return str.substring(0, (str.length() / 2));
        }
        return "Not a palindrom";
    }



}
